using System.Collections.Generic;
using System.Threading.Tasks;
using zencoffee.Business.Models;

namespace zencoffee.Business
{
    public interface ITableBusiness
    {
        Task<List<TableModel>> GetListTable();
    }
}