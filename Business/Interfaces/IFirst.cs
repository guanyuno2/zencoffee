using System.Threading.Tasks;

namespace zencoffee.Business.Interfaces
{
    public interface IFirst
    {
        Task<bool> Test();
    }
}