using System;
using MongoDB.Bson;
using zencoffee.Repositories.Models;

namespace zencoffee.Business.Models
{
    public class TableModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }

        long CreatedBy { get; set; }

        long UpdatedBy { get; set; }

        public static TableModel ToModel(MGTableModel model)
        {
            if (model == null) return null;
            return new TableModel()
            {
                Id = model.Id + "",
                Name = model.Name,
                Sequence = model.Sequence
            };
        }

        public static MGTableModel ToEntity(TableModel model)
        {
            if (model == null) return null;
            return new MGTableModel()
            {
                Id = ObjectId.TryParse(model.Id, out ObjectId id) ? id : default(ObjectId),
                Name = model.Name,
                Sequence = model.Sequence
            };
        }
    }
}