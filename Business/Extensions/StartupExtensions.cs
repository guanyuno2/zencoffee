using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using zencoffee.Business.Interfaces;
using zencoffee.Repositories;
using zencoffee.Repositories.Interfaces;
using zencoffee.Repositories.Models;

namespace zencoffee.Business.Extensions
{
    public static class StartupExtensions
    {
        public static void AddCoreService(
            this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSingleton<IMongoDb>(u =>
            {
                var mongoUrl = Configuration["Mongo"];
                var settings = MongoClientSettings.FromConnectionString(mongoUrl);
                settings.ServerApi = new ServerApi(ServerApiVersion.V1);
                var client = new MongoClient(settings);
                var database = client.GetDatabase("zencoffee");
                return new MongoDb(database);
            });
            services.AddSingleton<DbContext>();

            
            services.AddScoped<IFirst, FirstBusiness>();
            services.AddScoped<ITableBusiness, TableBusiness>();
            services.AddScoped<ITableRepository, TableRepository>();
        }
    }
}