using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace zencoffee.Business.Extensions
{
    [ApiController]
    [Produces("application/json")]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Conflict)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Forbidden)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.InternalServerError)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.MethodNotAllowed)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ClientApiError), (int)HttpStatusCode.Unauthorized)]
    public abstract class BasePublicController : ControllerBase
    {
      
        protected BasePublicController(
        )
        {
        }


        protected ActionResult OkOrNoContent(object value)
        {
            if (value == null) return NoContent();

            return Ok(value);
        }

        protected ActionResult NullSafeOk<T>(List<T> list)
        {
            if (list == null) return Ok(new List<T>());

            return Ok(list);
        }

        protected ActionResult NullSafeOk(object value)
        {
            if (value == null) throw new NotFoundException();

            return Ok(value);
        }

        protected ActionResult NullSafeOk() => new OkResult();
    }
}