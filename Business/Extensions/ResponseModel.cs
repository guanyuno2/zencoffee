using Newtonsoft.Json;

namespace zencoffee.Business.Extensions
{
    public class ErrorItem
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class ResponseModelHar<T> : ResponseModelHar
    {
        [JsonProperty("data")]
        public T Data { get; set; }
    }

    public class ResponseModelHar
    {
        [JsonProperty("error")]
        public bool Error { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        [JsonProperty("error_code", NullValueHandling = NullValueHandling.Ignore)]
        public string Error_Code { get; set; }


        public static ResponseModelHar Create<T>(
          T data,
          bool error = false,
          string message = null,
          string error_code = null)
        {
            ResponseModelHar<T> responseModel = new ResponseModelHar<T>();
            responseModel.Error = error;
            responseModel.Message = message;
            responseModel.Data = data;
            responseModel.Error_Code = error_code;
            return responseModel;
        }

        public static ResponseModelHar Create(bool error = false, string message = null, string error_code = null) => new ResponseModelHar()
        {
            Error = error,
            Message = message,
            Error_Code = error_code
        };
    }
}