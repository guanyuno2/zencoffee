using Microsoft.AspNetCore.Mvc;

namespace zencoffee.Business.Extensions
{
    public static class ResponseModelExtensions
    {
        public static IActionResult ToResult(this Controller controller, object data = null) 
        {
            string message1 = string.Empty;

            // return (IActionResult)controller.Json((object)ResponseModel<T>.Create(data, false, ""));
            return (IActionResult)controller.Json(data);
        }

        public static IActionResult ToResult(this Controller controller)
        {
            return (IActionResult) controller.Json("");
        }
    }
}