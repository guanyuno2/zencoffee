using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zencoffee.Business.Extensions
{
    public class ClientApiError
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("innerMessage")]
        public string InnerMessage { get; set; }

        [JsonProperty("errors")]
        public List<ErrorItem> Errors { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("errorTime")]
        public DateTime? ErrorTime { get; set; }

        [JsonProperty("stackTrace", NullValueHandling = NullValueHandling.Ignore)]
        public string StackTrace { get; set; }
    }
}