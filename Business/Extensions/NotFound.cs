using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;

namespace zencoffee.Business.Extensions
{
    [Serializable]
    public class NotFoundException : HttpStatusCodeException
    {
        public NotFoundException() : this("Resource not found")
        {
        }

        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override HttpStatusCode HttpStatusCode => HttpStatusCode.NotFound;
    }

    [Serializable]
    public class HttpStatusCodeException : Exception
    {
        public List<ErrorItem> Errors { get; set; }

        public virtual HttpStatusCode HttpStatusCode { get; }

        public HttpStatusCodeException()
        {
        }

        public HttpStatusCodeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HttpStatusCodeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public HttpStatusCodeException(string message) : base(message)
        {
        }

        public HttpStatusCodeException(ErrorItem error)
        {
            Errors = new List<ErrorItem> { error };
        }

        public HttpStatusCodeException(List<ErrorItem> errors) : base(errors?.FirstOrDefault()?.Message)
        {
            Errors = errors;
        }
    }
}