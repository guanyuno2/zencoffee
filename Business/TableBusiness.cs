using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using zencoffee.Business.Models;
using zencoffee.Repositories.Interfaces;

namespace zencoffee.Business
{
    public class TableBusiness : ITableBusiness
    {
        private readonly ITableRepository _repo;
        public TableBusiness(ITableRepository repo)
        {
            _repo = repo;
        }

        public async Task<List<TableModel>> GetListTable()
        {
            var entities = await _repo.GetListTable();
            return entities?.Select(TableModel.ToModel).ToList();
        }
    }
}