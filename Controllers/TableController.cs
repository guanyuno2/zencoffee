using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using zencoffee.Business;
using zencoffee.Business.Extensions;

namespace zencoffee.Controllers
{
    public class TableController : Controller
    {
        private readonly ITableBusiness _biz;
        public TableController(ITableBusiness biz)
        {
            _biz = biz;
        }

        [HttpGet("/tables")]
        public async Task<IActionResult> GetList()
        {
            var data = await _biz.GetListTable();
            return this.ToResult(data);
        }
    }
}