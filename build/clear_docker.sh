image_name="guanyuno2/zencoffee"

# Check if a container with the specified image name exists and stop and remove it
container_id=$(docker ps -a | grep $image_name | awk '{print $1}')
if [ -n "$container_id" ]; then
  docker stop $container_id && docker rm $container_id
fi

# Check if the image exists and remove it
image_id=$(docker images -q $image_name)
if [ -n "$image_id" ]; then
  docker rmi $image_id
fi