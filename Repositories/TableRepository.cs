using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using zencoffee.Repositories.Interfaces;
using zencoffee.Repositories.Models;

namespace zencoffee.Repositories
{
    public class TableRepository : ITableRepository
    {
        private readonly IMongoDb _context;
        private readonly IMongoCollection<MGTableModel> _collection;
        public TableRepository(IMongoDb context)
        {
            _context = context;
            _collection = _context.Db.GetCollection<MGTableModel>(typeof(MGTableModel).Name);
        }

        public Task<List<MGTableModel>> GetListTable()
        {
            return _collection.Find(Builders<MGTableModel>.Filter.Empty).ToListAsync();
        }
    }
}