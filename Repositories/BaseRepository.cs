// using System;
// using System.Collections.Generic;
// using System.Linq.Expressions;
// using System.Threading.Tasks;
// using MongoDB.Bson;
// using MongoDB.Driver;
// using zencoffee.Repositories.Interfaces;

// namespace zencoffee.Repositories
// {
//     public abstract class BaseRepository<T> where T : class
//     {
//         protected IBaseRepository<T> repo;
//         public IMongoCollection<T> collection
//         {
//             get { return repo.collection; }
//         }

//         public BaseRepository(IBaseRepository<T> repo)
//         {
//             this.repo = repo;
//         }

//         public Task Add(T entity)
//         {
//             return repo.Add(entity);
//         }

//         public Task Add(T entity, long createdBy)
//         {
//             return repo.Add(entity, createdBy);
//         }

//         public Task Delete(T entity)
//         {
//             return repo.Delete(entity);
//         }

//         public Task<T> GetById(ObjectId id)
//         {
//             if (id == null)
//                 throw new ArgumentException(nameof(id));

//             var filter = Builders<T>.Filter.Where(m => m.Id == id);

//             var data = collection.Find(filter).FirstOrDefaultAsync();
//             return data;
//         }

//         public Task<T> Task<T> GetById(string id)
//         {
//             if (id == null)
//                 throw new ArgumentException(nameof(id));
//             var oid = ObjectId.Parse(id);
//             var filter = Builders<T>.Filter.Where(m => m.OrgId == _orgId && m.SubCompanyId == _subCompanyId && m.Id == oid);

//             var data = collection.Find(filter).FirstOrDefaultAsync();
//             return data;
//         }

//         public Task<List<T>> GetById(IEnumerable<ObjectId> ids)
//         {
//             return repo.GetById(ids);
//         }

//         public Task<List<T>> GetById(List<string> ids)
//         {
//             return repo.GetById(ids);
//         }

//         public Task Update(T entity, params Expression<Func<T, object>>[] properties)
//         {
//             return repo.Update(entity, properties);
//         }

//         public Task<List<T>> GetManyByCondition(FilterDefinition<T> filter)
//         {
//             return repo.GetManyByCondition(filter);
//         }

//         public Task<T> GetOneByCondition(FilterDefinition<T> filter)
//         {
//             return repo.GetOneByCondition(filter);
//         }

//         public Task<long> Count(FilterDefinition<T> filter)
//         {
//             return repo.Count(filter);
//         }

//         public Task UpdateByCondition(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true)
//         {
//             return repo.UpdateByCondition(filter, update, updateMany, appendTrackingInfo);
//         }

//         public Task UpdateByCondition_Tracking(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true, long? updatedBy = null, DateTime? updatedAt = null)
//         {
//             return repo.UpdateByCondition_Tracking(filter, update, updateMany, appendTrackingInfo, updatedBy, updatedAt);
//         }


//         public Task DeleteByCondition(FilterDefinition<T> filter, bool deleteMany = false)
//         {
//             return repo.DeleteByCondition(filter, deleteMany);
//         }

//         public Task Delete(ObjectId id)
//         {
//             return repo.Delete(id);
//         }

//         public Task Delete(List<ObjectId> ids)
//         {
//             return repo.Delete(ids);
//         }

//         public virtual async Task<ValueTuple<List<T>, long>> GetWithLimit(int limit, int skip, List<FilterDefinition<T>> filters, SortDefinition<T> sort, ProjectionDefinition<T> project = null)
//         {
//             return await repo.GetWithLimit(limit, skip, filters, sort, project);
//         }

//         public async Task<ValueTuple<List<T>, long>> Paging(string query, int? page, int? pageSize, List<FilterDefinition<T>> filters = null, SortDefinition<T> sort = null, ProjectionDefinition<T> project = null, int? previewSize = null)
//         {
//             return await repo.Paging(query, page, pageSize, filters, sort, project, previewSize);
//         }
//     }
// }