using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace zencoffee.Repositories.Interfaces
{
    public interface IBaseRepository<T>
    {
        IMongoCollection<T> collection { get; }
        Task<T> GetById(ObjectId id);
        Task<T> GetById(string id);
        Task<List<T>> GetById(IEnumerable<ObjectId> ids);
        Task<List<T>> GetById(List<string> ids);
        Task<T> GetOneByCondition(FilterDefinition<T> filter);
        Task<List<T>> GetManyByCondition(MongoDB.Driver.FilterDefinition<T> filter);
        Task<long> Count(FilterDefinition<T> filter);
        Task Add(T entity);
        Task Add(T entity, long createdBy);
        Task Update(T entity, params Expression<Func<T, object>>[] properties);
        Task Delete(T entity);
        Task Delete(ObjectId id);
        Task Delete(List<ObjectId> ids);
        Task<ValueTuple<List<T>, long>> Paging(string query, int? page, int? pageSize, List<FilterDefinition<T>> filters = null, SortDefinition<T> sort = null, ProjectionDefinition<T> project = null, int? previewSize = null);
        Task UpdateByCondition(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true);
        Task UpdateByCondition_Tracking(FilterDefinition<T> filter, UpdateDefinition<T> update, bool updateMany = false, bool appendTrackingInfo = true, long? updatedBy = null, DateTime? updatedAt = null);
        Task DeleteByCondition(FilterDefinition<T> filter, bool deleteMany = false);
        Task<ValueTuple<List<T>, long>> GetWithLimit(int limit, int skip, List<FilterDefinition<T>> filters, SortDefinition<T> sort, ProjectionDefinition<T> project = null);
    }   
}