using System.Collections.Generic;
using System.Threading.Tasks;
using zencoffee.Repositories.Models;

namespace zencoffee.Repositories.Interfaces
{
    public interface ITableRepository
    {
        Task<List<MGTableModel>> GetListTable();
    }
}