using zencoffee.Repositories.Interfaces;

namespace zencoffee.Repositories.Models
{
    public class DbContext
    {
        private readonly IMongoDb db;
        public DbContext(IMongoDb db)
        {
            this.db = db;
        }
    }
}