using System;

namespace zencoffee.Repositories.Models
{
    public class TrackableEntity
    {
        DateTime CreatedAt { get; set; }

        DateTime UpdatedAt { get; set; }

        long CreatedBy { get; set; }

        long UpdatedBy { get; set; }
    }
}