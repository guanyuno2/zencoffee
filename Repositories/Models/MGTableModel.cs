using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace zencoffee.Repositories.Models
{
    [BsonIgnoreExtraElements]
    public class MGTableModel : TrackableEntity
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public int Sequence { get; set; }
    }
}